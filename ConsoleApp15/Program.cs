﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp15
{
    //Prooviks tee ja katseta

//class Inimene (nimi, sünniaasta)
    class Inimene //
    {
        private string _Nimi;
        public string Nimi
        { get
            { return _Nimi.Substring(0, 1).ToUpper() + _Nimi.Substring(1).ToLower(); }
        }
    private int _SünniAasta;
    public int SünniAasta { get { return _SünniAasta; } }

    public Inimene(string nimi, int sünniAasta)
        { _Nimi = nimi; _SünniAasta = sünniAasta; }

    public override string ToString() { return $"{Nimi} sünd: {SünniAasta}"; }
}
//class Õpilane : Inimene (kusklassis)
class Õpilane : Inimene
{
    public static List<Õpilane> Õpilased = new List<Õpilane>();
    public Klass KusÕpib = null;

    public Õpilane(string nimi, int sünniaasta) : base(nimi, sünniaasta) { Õpilased.Add(this); }

    public Õpilane(string nimi, int sünniaasta, Klass klass) : this(nimi, sünniaasta)
    { this.KusÕpib = klass; klass.Õpilased.Add(this); }

    public override string ToString()
        {
            return $" { KusÕpib?.Nimetus } õpilane {Nimi} sünd: {SünniAasta}";
        }
}

// class Õpetaja : Inimene (midaõpetab)
// class Õppeaine (nimetus, tunde)
// class Klass (nimetus, õpilased)
class Klass
{
    internal static List<Klass> Klassid = new List<Klass>();
    internal List<Õpilane> Õpilased = new List<Õpilane>();
    private string _Nimetus;
    public string Nimetus { get { return _Nimetus; } }

    public Klass(string nimetus) { _Nimetus = nimetus; Klassid.Add(this); }

    public Klass UusÕpilane(string nimi, int sünniaasta) { new Õpilane(nimi, sünniaasta, this); return this; }
    public Klass UusÕpilane(Õpilane uus) { uus.KusÕpib = this; Õpilased.Add(uus); return this; }

    public override string ToString()
    { return $"klass: { Nimetus} kus on { Õpilased.Count} õpilast"; }


}

// Siis tee Main meetodis

class Program
{
    public static void Main()
    {
        Klass k1B = new Klass("1B");
        k1B.UusÕpilane("Henn", 1955);
        k1B.UusÕpilane("Ants", 1960);
            new Õpilane("Juta", 1990);
        new Klass("2A")
            .UusÕpilane("Peeter", 1970)
            .UusÕpilane(new Õpilane("Jaagup", 1977))

        ;

            foreach (var x in Õpilane.Õpilased) Console.WriteLine(x);
            foreach (var x in Klass.Klassid) Console.WriteLine(x);
        }
}

/*
* lisa mõned õppeained
* lisa mõned õpetajad
* lisa mõned klassid
* lisa igasse klassi mõned õpilased

PRoovi välja trükkida
* kõik õppeained ja kes mida neist õpetab
* kõik klassi ja kes neis õpivad

MIllest alustada
* iga klassi jaoks
* * mõtle milliseid väljasid on vaja ja mis tüüpi nad peaks olema
* * mõtle kas ja milliseid staatilisi väljasid oleks vaja
* * mõtle kuidas teha konstruktor ja mitu neid vaja on
* iga ülesande jaoks
* * milliseid meetodeid oleks vaja
* * kus klassis nad peaks olema (võiks olla)
* * mis parameetreid nad vajaks
* * ehita nad valmis
*/


}
